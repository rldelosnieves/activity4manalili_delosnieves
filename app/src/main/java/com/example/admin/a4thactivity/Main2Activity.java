package com.example.admin.a4thactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;



public class Main2Activity extends AppCompatActivity {
    @Override
    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState );
        setContentView(R.layout.activity_main2);
        setTitle("Output");
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.set);
        TextView output = (TextView) findViewById(R.id.output2nd);
        output.setSingleLine(false);

        char samp;
        int x=0;
        int z;
        int ctr=0;
        String[] y = new String[message.length()];

        while(message.length()!=0){
            samp=message.charAt(0);
            for(z=0; z<message.length(); z++){
                if (message.charAt(z)== samp)
                    ctr++;
            }
            y[x] = String.valueOf(samp)+": "+String.valueOf(ctr)+"\n";
            message = message.replaceAll(String.valueOf(samp),"");
            x++;
            ctr=0;

        }

        for (z=0; z<x; z++){
            output.append(y[z]);
        }
    }
}
