package com.example.admin.a4thactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {
    public static final String set = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Input");
        Button button = (Button) findViewById(R.id.button);
        final EditText input = (EditText) findViewById(R.id.inputxt);
        final TextView output = (TextView) findViewById(R.id.outputxt);

        //**VALIDATION PURPOSES**//
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (input.getText().length() == 0)
                    output.setText("Enter Word:");
                else {
                    if (checklen(input.getText().length())) {
                        if(checks(input.getText().toString()))
                            sendMessage(null);

                        else
                            output.setText("String is invalid");
                    }
                }
            }

        });
    }

    public boolean checklen(int wrd){   //**Checking for the length of string must be less than 30**//
        if (wrd<=30)
            return true;
        else
            return false;
    }

    public boolean checks(String wrd){ //**For Special Characters**//
        return wrd.matches("[^0-9]*");
    }
    public void sendMessage(View view){
        Intent intent = new Intent (this,Main2Activity.class);       //**Change to next page **//
        TextView b = (TextView) findViewById(R.id.inputxt);
        String message = removow(b.getText().toString());
        intent.putExtra(set,message);
        startActivity(intent);

    }
    public String removow(String in){
        return in.replaceAll("[AEIOUaeiou]","");
    }
}